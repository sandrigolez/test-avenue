package br.com.sandrigo.testavenue.test;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.sandrigo.testavenue.data.DatabaseFiller;
import br.com.sandrigo.testavenue.entities.Image;
import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.entities.ProductDao;
import br.com.sandrigo.testavenue.util.StartHelper;

public class PersistenceTest {

	public PersistenceTest() {
	}

    @BeforeClass
    public static void initClass() {
        StartHelper.init(StartHelper.CONFIG_FILENAME_TEST);
    }

    @AfterClass
    public static void finish() {
        StartHelper.getInstance(DatabaseFiller.class).dropAllData();
    }


    @Test
	public void testProduct() {
		ProductDao productDao = StartHelper.getInstance(ProductDao.class);

		Product t1 = new Product("backpack 1");
		String id1 = productDao.save(t1);
		Assert.assertNotNull("ID expected", id1);

		List<Product> all = productDao.findAll();
		Assert.assertTrue("1 Product expected", all.size()==1);

		Product t2 = new Product("backpack 2");
		String id2 = productDao.save(t2);

		all = productDao.findAll();
		Assert.assertTrue("2 Products expected", all.size()==2);

		Product t1Loaded = productDao.findOne(id1);
		Assert.assertNotNull(t1Loaded);
		Assert.assertEquals(t1Loaded.getId(), id1);
		Assert.assertEquals(t1Loaded, t1);

		Product t2Loaded = productDao.findOne(id2);
		Assert.assertNotNull(t2Loaded);
		Assert.assertEquals(t2Loaded.getId(), id2);
		Assert.assertEquals(t2Loaded, t2);
	}

    @Test
    public void testImage() {
        ProductDao productDao = StartHelper.getInstance(ProductDao.class);

        List<Product> all = productDao.findAll();
        int size = all.size();

        Product t1 = new Product("candie");

        Image s1 = new Image();
        s1.setLink("http://www.festaexpress.com/Assets/Produtos/SuperZoom/Bala-jujuba-festabox_635881886914560232_635883017319315927.jpg");
        t1.addImage(s1);

        String id1 = productDao.save(t1);
        Assert.assertNotNull("ID expected", id1);
        String sid1 = s1.getId();

        all = productDao.findAll();
        Assert.assertTrue((size+1) + " Products expected", all.size()==size+1);

        Product productLoaded = productDao.findOne(id1);
        Assert.assertNotNull(productLoaded);
        Assert.assertNotNull(productLoaded.getImages());
        Assert.assertEquals(1, productLoaded.getImages().size());
        Image s1loaded = productLoaded.getImages().iterator().next();
        Assert.assertEquals(sid1, s1loaded.getId());

        System.out.println(productLoaded);
        System.out.println(s1loaded);

    }


    @Test
    public void testMultipleImage() {
        ProductDao productDao = StartHelper.getInstance(ProductDao.class);

        List<Product> all = productDao.findAll();
        int size = all.size();

        Product t1 = new Product("candies");

        Image s1 = new Image();
        s1.setLink("https://img.buzzfeed.com/buzzfeed-static/static/2015-07/29/14/campaign_images/webdr07/jujuba-x-bala-de-goma-a-verdadeira-polemica-que-d-2-1903-1438193435-0_dblbig.jpg");
        t1.addImage(s1);

        Image s2 = new Image();
        s2.setLink("http://www.festaexpress.com/Assets/Produtos/SuperZoom/Bala-jujuba-festabox_635881886914560232_635883017319315927.jpg");
        t1.addImage(s2);

        String id1 = productDao.save(t1);
        Assert.assertNotNull("ID expected", id1);
        String sid1 = s1.getId();
        String sid2 = s2.getId();

        all = productDao.findAll();
        Assert.assertTrue((size+1) + " Products expected", all.size()==size+1);

        Product productLoaded = productDao.findOne(id1);
        Assert.assertNotNull(productLoaded);
        Assert.assertNotNull(productLoaded.getImages());
        Assert.assertEquals(2, productLoaded.getImages().size());

        List<String> sidList = new ArrayList<>();
        sidList.add(sid1);
        sidList.add(sid2);

        Iterator<Image> subProductIterator = productLoaded.getImages().iterator();
        while (subProductIterator.hasNext()) {
            Image subProduct = subProductIterator.next();
            Assert.assertTrue(sidList.contains(subProduct.getId()));
            sidList.remove(subProduct.getId());
        }

    }

}
