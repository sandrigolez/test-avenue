package br.com.sandrigo.testavenue.test;

import org.junit.Assert;
import org.junit.Test;

import br.com.sandrigo.testavenue.entities.Image;
import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.entities.TopLevelEntity;

public class AnnotationTest {

    @Test
    public void annotationIsPresent() {
        Product t = new Product();

        boolean annotationPresent = t.getClass().isAnnotationPresent(TopLevelEntity.class);
        Assert.assertTrue(annotationPresent);
        Assert.assertEquals(t.getClass().getAnnotation(TopLevelEntity.class).discriminator(), TopLevelEntity.Discriminator.TASK);
    }

    @Test
    public void annotationIsNotPresent() {
        Image s = new Image();
        boolean annotationPresent = s.getClass().isAnnotationPresent(TopLevelEntity.class);
        Assert.assertFalse(annotationPresent);
    }
}
