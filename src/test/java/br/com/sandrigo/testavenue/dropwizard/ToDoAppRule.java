package br.com.sandrigo.testavenue.dropwizard;

import br.com.sandrigo.testavenue.util.StartHelper;
import io.dropwizard.testing.junit.ConfigOverride;
import io.dropwizard.testing.junit.DropwizardAppRule;

@SuppressWarnings("rawtypes")
public class ToDoAppRule extends DropwizardAppRule {

    @SuppressWarnings("unchecked")
	public ToDoAppRule(Class applicationClass, String configPath, ConfigOverride... configOverrides) {
        super(applicationClass, configPath, configOverrides);
        StartHelper.init(configPath);
    }


}
