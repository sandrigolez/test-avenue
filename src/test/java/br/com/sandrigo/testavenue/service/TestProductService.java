package br.com.sandrigo.testavenue.service;

import org.junit.*;

import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.entities.ProductDao;
import br.com.sandrigo.testavenue.service.FieldNames;
import br.com.sandrigo.testavenue.service.ProductService;
import br.com.sandrigo.testavenue.service.ServiceError;
import br.com.sandrigo.testavenue.service.ServiceErrorContext;
import br.com.sandrigo.testavenue.service.ServiceErrorMessage;
import br.com.sandrigo.testavenue.service.ServiceResult;
import br.com.sandrigo.testavenue.util.StartHelper;

import javax.persistence.EntityManager;

public class TestProductService {

	ServiceError errorNameEmpty = new ServiceError(new ServiceErrorContext(FieldNames.PRODUCT_NAME), ServiceErrorMessage.PRODUCT_PRODUCTNAME_EMPTY);
    ServiceError errorNameTooShort = new ServiceError(new ServiceErrorContext(FieldNames.PRODUCT_NAME), ServiceErrorMessage.PRODUCT_PRODUCTNAME_TOO_SHORT);

    private EntityManager entityManager;

    @BeforeClass
    public static void initClass() {
        StartHelper.init(StartHelper.CONFIG_FILENAME_TEST);
    }

    @Before
    public void startTx() {
        if (entityManager==null) {
            entityManager = StartHelper.getInstance(EntityManager.class);
        }
        entityManager.getTransaction().begin();

    }

    @After
    public void rollbackTx() {
        entityManager.getTransaction().rollback();
        errorNameTooShort.getContext().setId(null);
    }

    @Test
    public void emptyNameShouldGiveError() {
        ProductService service = StartHelper.getInstance(ProductService.class);
        ProductDao dao = StartHelper.getInstance(ProductDao.class);

        Product l = new Product();
        ServiceResult<Product> result = service.createNewEntity(l);

        Assert.assertTrue(result.hasErrors());
        Assert.assertEquals(1, result.getErrors().size());
        Assert.assertTrue(result.containsError(errorNameEmpty));
        Assert.assertEquals(0, dao.findAll().size());

        l.setName("a name");
        ServiceResult<Product> result2 = service.createNewEntity(l);

        Assert.assertFalse(result2.hasErrors());

    }


    @Test
    public void saveProductWithoutErrorButWithErrorOnUpdate() {
        ProductService service = StartHelper.getInstance(ProductService.class);
        ProductDao dao = StartHelper.getInstance(ProductDao.class);

        Product l = new Product("name");
        ServiceResult<Product> result = service.createNewEntity(l);

        Assert.assertFalse(result.hasErrors());
        Assert.assertEquals(0, result.getErrors().size());
        Assert.assertEquals(1, dao.findAll().size());

        l.setName("aa");
        ServiceResult<Product> updateResult = service.updateEntity(l);
        Assert.assertTrue(updateResult.hasErrors());
        Assert.assertEquals(1, updateResult.getErrors().size());
        errorNameTooShort.getContext().setId(l.getId());
        Assert.assertTrue(updateResult.containsError(errorNameTooShort));
        Assert.assertEquals(1, dao.findAll().size());


    }


}
