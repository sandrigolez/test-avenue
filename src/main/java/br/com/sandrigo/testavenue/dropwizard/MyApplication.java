package br.com.sandrigo.testavenue.dropwizard;

import java.util.EnumSet;
import java.util.Properties;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration.Dynamic;

import org.eclipse.jetty.servlets.CrossOriginFilter;

import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.hubspot.dropwizard.guice.GuiceBundle;

import br.com.sandrigo.testavenue.data.DatabaseFiller;
import br.com.sandrigo.testavenue.resources.ProductResource;
import br.com.sandrigo.testavenue.util.StartHelper;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MyApplication extends Application<MyConfiguration> {

    private GuiceBundle<MyConfiguration> guiceBundle;

    public static void main(String[] args) throws Exception {
        for (int i = 0; i < args.length; i++) {
            if (args[i].endsWith(".yml")) {
                StartHelper.setConfigFilename(args[i]);
            }
        }
        new MyApplication().run(args);
    }

    @Override
    public String getName() {
        return "test-avenuecode";
    }

    @Override
    public void initialize(Bootstrap<MyConfiguration> bootstrap) {

        MyConfiguration configuration = StartHelper.createConfiguration(StartHelper.getConfigFilename());
        Properties jpaProperties = StartHelper.createPropertiesFromConfiguration(configuration);

        JpaPersistModule jpaPersistModule = new JpaPersistModule(StartHelper.JPA_UNIT);
        jpaPersistModule.properties(jpaProperties);

        guiceBundle = GuiceBundle.<MyConfiguration>newBuilder()
                .addModule(new MyGuiceModule())
                .addModule(jpaPersistModule)
                .enableAutoConfig("br.com.sandrigo.testavenue")
                .setConfigClass(MyConfiguration.class)
                .build();

        bootstrap.addBundle(guiceBundle);
    }

    @Override
    public void run(MyConfiguration configuration, Environment environment) {

        environment.servlets().addFilter("persistFilter", guiceBundle.getInjector().getInstance(PersistFilter.class)).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");

        StartHelper.init(StartHelper.getConfigFilename());
        DatabaseFiller databaseFiller = StartHelper.getInjector().getInstance(DatabaseFiller.class);
        databaseFiller.initData();

        Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location");
        filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

        environment.jersey().register(guiceBundle.getInjector().getInstance(ProductResource.class));
    }

}
