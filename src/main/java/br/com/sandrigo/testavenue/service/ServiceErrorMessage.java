package br.com.sandrigo.testavenue.service;

public enum ServiceErrorMessage {

    PRODUCT_PRODUCTNAME_EMPTY,
    PRODUCT_PRODUCTNAME_TOO_SHORT,

    IMAGE_LINK_EMPTY

}
