package br.com.sandrigo.testavenue.service;

import com.google.inject.Inject;

import br.com.sandrigo.testavenue.entities.GenericDAOUUID;
import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.entities.ProductDao;
import br.com.sandrigo.testavenue.validation.IEntityValidator;
import br.com.sandrigo.testavenue.validation.ProductValidator;

public class ProductService extends TopLevelEntityService<Product> {
	
	@Inject
    ProductDao dao;

	@Override
	public GenericDAOUUID<Product, String> getDao() {
		return dao;
	}

	@Override
	public IEntityValidator buildEntityValidator(Product entity) {
		return new ProductValidator(entity);
	}

}
