package br.com.sandrigo.testavenue.service;

public interface FieldNames {

    public static final String PRODUCT_NAME = "product.name";
    public static final String IMAGE_LINK = "image.link";

}
