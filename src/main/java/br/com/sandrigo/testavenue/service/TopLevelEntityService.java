package br.com.sandrigo.testavenue.service;

import java.util.List;
import java.util.NoSuchElementException;

import br.com.sandrigo.testavenue.entities.BaseEntityUUID;
import br.com.sandrigo.testavenue.entities.GenericDAOUUID;
import br.com.sandrigo.testavenue.validation.IEntityValidator;

public abstract class TopLevelEntityService<T extends BaseEntityUUID> {

    public abstract GenericDAOUUID<T, String> getDao();

    public ServiceResult<T> createNewEntity(T entityData) {

        IEntityValidator validator = buildEntityValidator(entityData);
        List<ServiceError> errorMessages = validator.validateForCreation();
        if (errorMessages.size() == 0) {
            T entity = entityData;
            getDao().save(entity);
        }

        return new ServiceResult<>(entityData, errorMessages);
    }

    public ServiceResult<T> updateEntity(T entityData) {

        IEntityValidator validator = buildEntityValidator(entityData);
        List<ServiceError> errorMessages = validator.validateForUpdate();
        if (errorMessages.size() == 0) {
            T entity = entityData;
            getDao().update(entity);
        }
        return new ServiceResult<>(entityData, errorMessages);
    }

    public void deleteEntity(String id) {
        T entity = getDao().findOne(id);
        if (entity==null) {
            throw new NoSuchElementException();
        }
        getDao().delete(entity);
    }

    public abstract IEntityValidator buildEntityValidator(T entity);

}
