package br.com.sandrigo.testavenue.validation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.service.FieldNames;
import br.com.sandrigo.testavenue.service.ServiceError;
import br.com.sandrigo.testavenue.service.ServiceErrorContext;
import br.com.sandrigo.testavenue.service.ServiceErrorMessage;

public class ProductValidator implements IEntityValidator {
	
	private final Product product;

    private final int nameMinLenght = 3;

    public ProductValidator(Product product) {
        if (product == null) {
            throw new RuntimeException("br.com.sandrigo.testavenue.productvalidator.NoProductGiven");
        }
        this.product = product;
    }

    public List<ServiceError> validateForCreation() {
        List<ServiceError> errors = new ArrayList<>();
        errors.addAll(validateRequiredFields());
        
        return errors;
    }

    public List<ServiceError> validateRequiredFields() {
        
    	List<ServiceError> errorMessages = new ArrayList<>();
        String id = product.getId();
        
        if (id==null) {
            id = product.getValidationId();
        }
        if (StringUtils.isEmpty(product.getName())) {
            errorMessages.add(new ServiceError(new ServiceErrorContext(FieldNames.PRODUCT_NAME, id), ServiceErrorMessage.PRODUCT_PRODUCTNAME_EMPTY));
        }
        else if (StringUtils.length(product.getName()) < nameMinLenght) {
        	errorMessages.add(new ServiceError(new ServiceErrorContext(FieldNames.PRODUCT_NAME, id), ServiceErrorMessage.PRODUCT_PRODUCTNAME_TOO_SHORT));
        }

        return errorMessages;
    }

    public List<ServiceError> validateForUpdate() {

        List<ServiceError> errors = new ArrayList<>();
        errors.addAll(validateRequiredFields());

        return errors;
	}

}
