package br.com.sandrigo.testavenue.validation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.sandrigo.testavenue.entities.Image;
import br.com.sandrigo.testavenue.service.FieldNames;
import br.com.sandrigo.testavenue.service.ServiceError;
import br.com.sandrigo.testavenue.service.ServiceErrorContext;
import br.com.sandrigo.testavenue.service.ServiceErrorMessage;

public class ImageValidator {

	private final Image entity;

    public ImageValidator(Image entity) {
        if (entity == null) {
            throw new RuntimeException("br.com.sandrigo.testavenue.taskvalidator.NoImageGiven");
        }
        this.entity = entity;
    }

    public List<ServiceError> validateForCreation() {
        List<ServiceError> errors = new ArrayList<>();

        errors.addAll(validateRequiredFields());

        return errors;

    }

    public List<ServiceError> validateRequiredFields() {
        List<ServiceError> errorMessages = new ArrayList<>();

        String id = entity.getId();
        if (id==null) {
            id = entity.getValidationId();
        }
        if (StringUtils.isEmpty(entity.getLink())) {
            errorMessages.add(
                    new ServiceError(
                            new ServiceErrorContext(FieldNames.IMAGE_LINK, id),
                            ServiceErrorMessage.IMAGE_LINK_EMPTY
                    )
            );
        }

        return errorMessages;
    }

}
