package br.com.sandrigo.testavenue.validation;

import java.util.Collection;
import java.util.List;

import br.com.sandrigo.testavenue.service.ServiceError;

public interface IEntityValidator {

    public Collection<ServiceError> validateRequiredFields();

    public List<ServiceError> validateForUpdate();

    public List<ServiceError> validateForCreation();
}
