package br.com.sandrigo.testavenue.resources;

import java.net.URI;
import java.util.NoSuchElementException;

import javax.persistence.OptimisticLockException;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.com.sandrigo.testavenue.entities.BaseEntityUUID;
import br.com.sandrigo.testavenue.entities.GenericDAOUUIDImpl;
import br.com.sandrigo.testavenue.service.ServiceResult;
import br.com.sandrigo.testavenue.service.TopLevelEntityService;

public abstract class ResourceHelper {

    private static final String TYPE_TEXT = "text/plain";
	private static final String TYPE_JSON = "text/json";

	public static Response create(BaseEntityUUID entity, TopLevelEntityService service) {
        try {
            ServiceResult<BaseEntityUUID> serviceResult = service.createNewEntity(entity);
            if (serviceResult.hasErrors()) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .type(TYPE_JSON)
                        .entity(serviceResult.getErrors()).build();
            }
            return Response.created(new URI(serviceResult.getResult().getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).type(TYPE_TEXT)
                    .entity(e.getMessage()).build();
        }
    }

    public static Response update(String id, BaseEntityUUID entity, TopLevelEntityService service) {
        if (entity.getId() == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        try {
            ServiceResult<BaseEntityUUID> serviceResult = service.updateEntity(entity);
            if (serviceResult.hasErrors()) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .type(TYPE_JSON)
                        .entity(serviceResult.getErrors()).build();
            }
        } catch (OptimisticLockException e) {
            Logger.getLogger(ResourceHelper.class).warn("OptimisticLockException", e);
            return Response.status(Response.Status.BAD_REQUEST).tag("OptimisticLockException").build();
        }
        return Response.status(Response.Status.ACCEPTED).entity(entity).build();
    }


    public static Response get(String id, GenericDAOUUIDImpl dao) {
        BaseEntityUUID entity = dao.findOne(id);
        if (entity != null) {
            return Response.ok(entity).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    public static Response delete(String id, TopLevelEntityService service) {
        try {
            service.deleteEntity(id);
        } catch (NoSuchElementException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().build();
    }
}
