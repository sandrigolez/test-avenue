package br.com.sandrigo.testavenue.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.inject.Inject;

import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.entities.ProductDao;
import br.com.sandrigo.testavenue.service.ProductService;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductResource {

	@Inject
	private ProductDao dao = null;

	@Inject
	private ProductService service = null;

	public ProductResource() {}

	@GET
	public List<Product> list() {
		List<Product> ret = dao.findAll();
		return ret;
	}

    @GET
    @Path("/{id}")
	public Response get(@PathParam("id") String id) {
        return ResourceHelper.get(id, dao);
	}

	@POST
	public Response create(Product t) {
		return ResourceHelper.create(t, service);
	}


	@PUT
	@Path("{id}")
	public Response update(@PathParam("id") String id, Product t) {
		return ResourceHelper.update(id, t, service);
	}


    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") String id) {
        return ResourceHelper.delete(id, service);
    }

}
