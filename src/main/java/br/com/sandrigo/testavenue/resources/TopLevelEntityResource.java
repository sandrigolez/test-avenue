package br.com.sandrigo.testavenue.resources;

import java.net.URI;

import javax.persistence.OptimisticLockException;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.com.sandrigo.testavenue.entities.BaseEntityUUID;
import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.service.ServiceResult;
import br.com.sandrigo.testavenue.service.TopLevelEntityService;

public abstract class TopLevelEntityResource {

    private static final String TYPE_TEXT = "text/plain";
	private static final String TYPE_JSON = "text/json";

	public Response create(BaseEntityUUID entity, TopLevelEntityService service) {
        try {
            ServiceResult<Product> serviceResult = service.createNewEntity(entity);
            if (serviceResult.hasErrors()) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .type(TYPE_JSON)
                        .entity(serviceResult.getErrors()).build();
            }
            return Response.created(new URI(serviceResult.getResult().getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).type(TYPE_TEXT)
                    .entity(e.getMessage()).build();
        }
    }

    public Response update(String id, BaseEntityUUID entity, TopLevelEntityService service) {
        if (entity.getId()==null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        try {
            ServiceResult<Product> serviceResult = service.updateEntity(entity);
            if (serviceResult.hasErrors()) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .type(TYPE_JSON)
                        .entity(serviceResult.getErrors()).build();
            }
        } catch (OptimisticLockException e) {
            Logger.getLogger(this.getClass()).warn("OptimisticLockException", e);
            return Response.status(Response.Status.BAD_REQUEST).tag("OptimisticLockException").build();
        }
        return Response.status(Response.Status.ACCEPTED).entity(entity).build();
    }
}
