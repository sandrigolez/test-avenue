package br.com.sandrigo.testavenue.data;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.google.inject.persist.Transactional;

import br.com.sandrigo.testavenue.entities.Image;
import br.com.sandrigo.testavenue.entities.Product;
import br.com.sandrigo.testavenue.service.ProductService;
import br.com.sandrigo.testavenue.service.ServiceResult;
import br.com.sandrigo.testavenue.util.StartHelper;

/**
 * Class to fill the database with some sample entities.
 */
public class DatabaseFiller {

	Logger logger = Logger.getLogger(DatabaseFiller.class);

	static DatabaseFiller instance = null;

	@Transactional
	public void initData() {
        addProducts();
	}

    private void addProducts() {

    	ProductService productService = StartHelper.getInstance(ProductService.class);
    	Product p = new Product("Dell 17-Inch Tek Backpack (NPJXM)");
    	Image image = new Image();
    	image.setLink("https://images-na.ssl-images-amazon.com/images/G/01/aplusautomation/vendorimages/7afb5bca-3dd7-43af-a262-7f4602d5dc57.jpg._CB316888092_.jpg");
    	p.addImage(image);
    	ServiceResult<Product> productResult = productService.createNewEntity(p);

    	if (productResult.hasErrors()) {
            throw new RuntimeException(productResult.getErrors().toString());
        }

    	Product p2 = new Product("Case Logic VNB-217 Value 17-Inch Laptop Backpack (Black)");
    	Image image2 = new Image();
    	image2.setLink("http://images.prod.meredith.com/product/c382885fafd6a15d92ae105151a6528a/8a208a964986327bdb7798084c1092995a3b48693ab30ee0ff0e4c0fc9acf111/l/case-logic-vnb-217-value-17-inch-laptop-backpack-black");
    	p2.addImage(image2);
    	ServiceResult<Product> productResult2 = productService.createNewEntity(p);

    	if (productResult2.hasErrors()) {
            throw new RuntimeException(productResult2.getErrors().toString());
        }
    }

    @Transactional
    public void dropAllData() {
        EntityManager em = StartHelper.getInstance(EntityManager.class);
        em.createNativeQuery("TRUNCATE SCHEMA public AND COMMIT NO CHECK").executeUpdate();
    }
}
