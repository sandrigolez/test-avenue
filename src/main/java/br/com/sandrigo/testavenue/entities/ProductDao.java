package br.com.sandrigo.testavenue.entities;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

public class ProductDao extends GenericDAOUUIDImpl<Product, String> {

	@Inject
	public ProductDao(Provider<EntityManager> emf) {
		super(emf);
		entityClass=Product.class;
	}
	
	public Product findByExactName(String name) {
		Product t = (Product) getEntityManager()
        		.createNativeQuery("SELECT * FROM Product p where lower(p.name) = :value ", Product.class).setParameter("value", name.toLowerCase()).getSingleResult();
        return t;
    }


    @Override
    @Transactional
    public void update(Product entity) {
        super.update(entity);
    }

    @Override
    @Transactional
    public String save(Product entity) {
        return super.save(entity);
    }

}
