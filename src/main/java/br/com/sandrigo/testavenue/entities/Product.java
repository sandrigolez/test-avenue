package br.com.sandrigo.testavenue.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@TopLevelEntity(discriminator = TopLevelEntity.Discriminator.PRODUCT)
@Audited
@NamedQueries({@NamedQuery(name="Product.GetAll", query = "from Product c")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product extends BaseEntityUUID {

	private static final long serialVersionUID = 4168245947122172412L;

	@Column(name="name", nullable= false, length = 80)
	private String name;

	@OneToMany(mappedBy="product", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<Image> images;

	@ManyToOne
	@JoinColumn(name = "product_parent_id")
    private Product parent;

    @OneToMany(mappedBy="parent", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Product> children;

    public Product() {
	}

    public Product(String name) {
    	this.name = name;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	public List<Product> getChildren() {
		return children;
	}

	public void setChildren(List<Product> children) {
		this.children = children;
	}

	public boolean addImage(Image image) {
		if(this.images == null) {
			this.images = new ArrayList<>();
		}
		return this.images.add(image);
	}

}
