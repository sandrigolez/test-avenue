package br.com.sandrigo.testavenue.entities;

import javax.validation.constraints.NotNull;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TopLevelEntity {

    public enum Discriminator {
        TASK,
        LANGUAGE,
        PRODUCT
    }

    @NotNull
    Discriminator discriminator();

}
