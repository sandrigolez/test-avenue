test-avenuecode
=========================
This project is a test for Avenue Code : REST application written in Java using HSQLDB + Hibernate + JPA 2.1 + JAX-RS.

- built on [Dropwizard](https://dropwizard.github.io/dropwizard/) version 0.7.0
- dependency injection with [Google Guice](https://code.google.com/p/google-guice/) (no Spring dependencies!)
- [Hibernate](http://hibernate.org/) / JPA 2.1 as database access framework
- [HSQLDB](http://hsqldb.org/) as database
- read database configuration from Dropwizard yaml config file, persistence.xml is not used
- "Session-per-HTTP-request" with Guice [PersistentFilter](https://code.google.com/p/google-guice/wiki/JPA)
- suport for [cross-origin resource sharing](http://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
- JPA entities with [UUIDs](http://en.wikipedia.org/wiki/Universally_Unique_Identifier) as primary keys
- Auditing/Version control of entities with [hibernate envers](http://envers.jboss.org/)
- a pattern for accessing and manipulation entities with HTTP REST calls (Resource => Service => DAO => entity)
- a pattern for ServiceResult objects which contain ServiceErrorMessages (which can later be bound to web form fields in the client)

### Summary ###

* Installation
* Running tests
* Executing the project

### Installation ###
- clone this project to your local repository
- move to your project home directory
- execute the command: mvn clean install

### Running tests ###
- execute the command: mvn tests

### Executing the project ###
- Start the application with the class "MyApplication" (main class) with the parameters "server test.yml"

# Usage

* List all products with:

        GET => http://localhost:8080/products

* Add a new product with:

        POST => http://localhost:8080/products

    Header:

        Content-Type:application/json

    JSON-Body e.g. :

        {"name" : "Dell 17-Inch Tek Backpack (NPJXM)", "images": [ {"link" : "https://images-na.ssl-images-amazon.com/images/G/01/aplusautomation/vendorimages/7afb5bca-3dd7-43af-a262-7f4602d5dc57.jpg._CB316888092_.jpg"} ]}

* Modify a product:

        PUT => http://localhost:8080/products/[id]

    Header:

        Content-Type:application/json
        Accept:application/json

    JSON-Body e.g.:

        {
        "id": "502830944684600101468760d9ea0000",
        "name": "NEW Dell 17-Inch Tek Backpack (NPJXM)"
        }

* Remove a product:

        DELETE => http://localhost:8080/products/[id]

* List all revision numbers of a product with:

        GET => http://localhost:8080/products/[id]/revisions

* Show a single product with:

        GET => http://localhost:8080/products/[id]

I recommend you use the **chrome extension [Postman](http://getpostman.com)** to make such HTTP calls!